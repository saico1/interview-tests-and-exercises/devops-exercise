#!/bin/bash

set -e
set -u
#set -x

## Set variables
TODAY=`date +"%d%b%Y"`
DB_BACKUP_PATH='/backup/mongo'
MYSQL_HOST=''
MYSQL_PORT='3306'
MMYSQL_USER=''
MMYSQL_PASSWD=''
DATABASE_NAME=''

# Create todays backups directory
mkdir -p ${DB_BACKUP_PATH}/${TODAY}

mysqldump -h ${MYSQL_HOST} \
   -P ${MYSQL_PORT} \
   -u ${MYSQL_USER} \
   -p${MYSQL_PASSWORD} \
   ${DATABASE_NAME} | gzip > ${DB_BACKUP_PATH}/${TODAY}/${DATABASE_NAME}-${TODAY}.sql.gz

if [ $? -eq 0 ]; then
  echo "Database backup successfully completed"
else
  echo "Error found during backup"
  exit 1
fi


##### Remove backups older than {BACKUP_RETAIN_DAYS} days  #####

DBDELDATE=`date +"%d%b%Y" --date="${BACKUP_RETAIN_DAYS} days ago"`

if [ ! -z ${DB_BACKUP_PATH} ]; then
      cd ${DB_BACKUP_PATH}
      if [ ! -z ${DBDELDATE} ] && [ -d ${DBDELDATE} ]; then
            rm -rf ${DBDELDATE}
      fi
fi

## Restore

#gunzip ${DB_BACKUP_PATH}/select_day/${DATABASE_NAME}-select_day.sql.gz
#mysql -h ${MYSQL_HOST} \
#   -P ${MYSQL_PORT} \
#   -u ${MYSQL_USER} \
#   -p${MYSQL_PASSWORD} \
#   ${DATABASE_NAME} < ${DB_BACKUP_PATH}/select_day/${DATABASE_NAME}-select_day.sql

