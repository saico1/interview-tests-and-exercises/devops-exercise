#! /usr/bin/env bash
 
set -e
set -u
#set -x

## Set variables
TODAY=`date +"%d%b%Y"`
DB_BACKUP_PATH='/backup/mongo'
MONGO_HOST=''
MONGO_PORT='27017'
MONGO_USER=''
MONGO_PASSWD=''

# Create todays backups directory
mkdir -p ${DB_BACKUP_PATH}/${TODAY}


AUTH_PARAM=" --username ${MONGO_USER} --password ${MONGO_PASSWD} "

# Run backup
mongodump --host ${MONGO_HOST} --port ${MONGO_PORT} ${AUTH_PARAM} --out ${DB_BACKUP_PATH}/${TODAY}/
 
## RESTORE

# mongorestore --host ${MONGO_HOST} --port ${MONGO_PORT} ${AUTH_PARAM} --verbose \path\dump
