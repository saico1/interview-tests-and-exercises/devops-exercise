#! /usr/bin/env bash

set -e
set -u
#set -x

# unzip and parse logs
LOG=`/usr/bin/gunzip -c ./log.gz`

## Count events (events names shouldn't be space separated )
echo "Events count:"
echo $LOG | awk '{print $3}' | uniq -c

## Count sms by destination
echo "Number of messages by destination:"
echo $LOG | grep "Sent SMS" | awk '{print $7}' | uniq -c

## Count character by FID

if [ -z ${FID} ] ; then
  FID=123456789
fi

MESSAGE=`echo $LOG | grep "[FID:${FID}]" | grep -o "msg.*" | cut -d ':' -f3 | cut -d ']' -f1` 

# Separate message to avoid '\n' be counted as character
COUNT=`echo -n $MESSAGE | wc -m`
echo "Message FID:${FID} has ${COUNT} characters"

