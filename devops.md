# Ejercicio DevOps
─
## Ejercicio 1: Bash
Se requiere construir un archivo en bash que compla los siguientes requisitos:
1) El script deberá hacer una llamada usando ​ curl a
un endpoint.
2) El endpoint podrá ser especificado por variable de entorno, parámetro o ingresado
por standard input, y deberá contemplarse en el orden mencionado.
3) la respuesta de dicha llamada, deberá imprimirse en el standard output.

## Ejercicio 2: Bash & Procesamiento de logs

Dado el siguiente archivo de texto comprimido con GNU zip, el cual su formato es el
siguiente:
“​ fecha” “hora” “evento” “nombre de conexión” “identificador de transacción” “origen”
“destino” “mensaje”
Ejemplo:
2020-08-01 17:00:00 Sent SMS [SMSC:conn1] [FID:123456789] [from:4321]
[to:123443211234] [msg:11:Hello World]
Se requiere que escriba una secuencia de comandos que permita:
1) Determinar qué cantidad de eventos de cada tipo hay registrados en el archivo.
2) Filtrando por el ​ evento​ “Sent SMS”, determinar cuántos mensajes totales por
destinatario fueron registrados.
3) Dado un ​ id de transacción​ específico, determine cuántos caracteres tiene el ​ mensaje

## Ejercicio 3: Network
Suponiendo lo siguiente:

**Equipo 1:**

``` 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
inet 4.4.4.4 netmask 255.255.255.0 broadcast 4.4.4.255 Dynamo Mobile 2

eth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
inet 192.168.0.2 netmask 255.255.255.0 broadcast 192.168.0.255
```  
**Equipo 2:**

``` 
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
inet 5.5.5.5 netmask 255.255.255.0 broadcast 5.5.5.5.255
eth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
inet 192.168.0.3 netmask 255.255.255.0 broadcast 192.168.0.255
```  

**Equipo Cliente:**

```  
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
inet 8.8.8.8 netmask 255.255.255.0 broadcast 8.8.8.255
```  

Para realizar este ejercicio, el e ​ quipo 2​ se conecte al endpoint del e ​ quipo cliente​ al puerto
1234 a través del ​ equipo 1​ . Describa y escriba qué comando/s deben realizarse para poder
establecer esta conexión y que quede registrado de manera permanente.

## Ejercicio 4: Docker

Se requiere armar un archivo para construir una imagen de Docker partiendo la última
versión estable de CentOS o Ubuntu. Luego es necesario, utilizando el script desarrollado
en el ejercicio 1, este debe iniciarse automáticamente cuando se inicie la imagen de docker.

## Ejercicio 5: Orquestador

Se requiere que, utilizando el orquestador de contenedores que más
experiencia/conocimiento cuentes, construyas un stack configurando una imagen de
docker con nginx como servicio. Ejecute la imagen del punto 4 dentro del stack pasando
como endpoint el servicio montado. Como resultado, de esta ejecución,

## Ejercicio 6: Backup & Restore

Desarrolle un script para realizar backups sobre base de datos MySQL y MongoDB. Puede
seleccionar la estrategia que desee, con cualquier herramienta de código abierto que crea
necesaria para su óptima implementación. Adicionalmente deberá especificar cómo
realizar el restore del mismo.

## Ejercicio 7: Integración continua

Se requiere utilizar alguna solución que permita el deploy automático de un repositorio git.
Sugerimos usar algo como pipelines de bitbucket o gitlab.
Se solicita que especifique qué configuración aplicar y el yml correspondiente para la
implementación sobre cualquiera de estas plataformas

## Entrega

La entrega se puede realizar compartiendo los ejercicios por correo electrónico o
repositorio de código que cuentes. Los ejercicios debes separarlos en diferentes
directorios y es necesario que puedan correr en un ambiente de código abierto.
Junto a la entrega debes sumar un README especificando las tecnologías utilizadas y una
descripción/aclaraciones necesarias para poder interpretar correctamente el trabajo
realizado.
