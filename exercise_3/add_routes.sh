#! /usr/bin/env bash

set -e
set -u
#set -x

## Add route to interfaces
echo -e "#persistent static routes\nup route add -net 8.8.8.8 gw 192.168.0.2 dev eth1" | tee -a /etc/network/interfaces

## Restart service
sudo systemctl restart network
