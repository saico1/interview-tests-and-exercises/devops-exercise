provider "kubernetes" {
  config_path   = "~/.kube/config"
}

terraform {
  required_version = ">=0.12.0"
}

