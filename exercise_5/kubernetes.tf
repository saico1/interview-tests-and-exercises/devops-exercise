## Namespace
resource "kubernetes_namespace" "testing-ns" {
  metadata {
    name = "testing"
  }
}

## Nginx Pod
resource "kubernetes_pod" "echo" {
  metadata {
    name = "frontend"
    namespace = "testing"
    labels = { App = "echo" }
  }
  spec {
    container {
      image = "nginxdemos/hello"
      name  = "frontend"
      port {
        container_port = 80
      } 
    }
  }
}

## Exercise4 Pod
resource "kubernetes_pod" "bash" {
  metadata {
    name = "backend"
    namespace = "testing"
    labels = { App = "bash" }
  }
  spec {
    
    image_pull_secrets {
      name = "regcred"
    }
    container {
      image = "registry.gitlab.com/saico1/interview-tests-and-exercises/devops-exercise/exercise4"
      name  = "backend"
      port {
        container_port = 22
      } 
      env {
        name = "ENDPOINT"
        value = kubernetes_service.echo.load_balancer_ingress.0.ip
      }
    }
  }
}
## Service
resource "kubernetes_service" "echo" {
  metadata {
    name = "echo-example2"
    namespace = "testing"
  }
  spec {
    selector = {
      App = kubernetes_pod.echo.metadata.0.labels.App
    }
    port {
      port        = 80
      target_port = 80
    }
    type = "LoadBalancer"
  }
}

output "lb_ip" {
  value = kubernetes_service.echo.load_balancer_ingress.0.ip
}
