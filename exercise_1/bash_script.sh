#! /usr/bin/env bash

set -e
#set -u
#set -x

## Test api with curl
if [ -z ${ENDPOINT} ] ; then
  if [ -z $1 ] ; then
    echo "Endpoint not defined. usage: define env var ENDPOINT or pass it as argument: bash_script.sh https://some-endpoint"
    exit 1
  else
    ENDPOINT=$1
  fi
fi

while true
do
  curl ${ENDPOINT}
  sleep 3600
done
