# Devops exercises

This repo cointais solutions to job interview exercises listed on [this](./devops.md) file.

For this examples were used:
- bash scripting
- Terraform > 12
- digital ocean account
- ansible < 2.9

## Exercise 1

Bash script to curl some endpoint, if endpoint is not provided by env variable or argument shows an error

## Exercise 2

Bash script to parse compressed log file. If FID is not provided by env it uses the only FID available in example

## Exercise 3

Bash script which contains instructions to add estatic routes to a Debian/Ubuntu system.


Only for reference ** DO NOT USE. **

## exercise 4

Dockerfile to create an image wich runs exercise 1 bash script at start, build and published on gitlab's registry throught pipeline.

## Exercise 5

Terraform code to deploy on digital a k8s node with nginx and exercise 4 pods, the last one consumes the first through loadbalancer

## Exercise 6
Mysql and mongodb scripts contains restore steps commented inside file.

## Exercise 7
A .gitlab-ci.yml pipeline which build and publish docker image of exercise 4 was created on this repo, it also makes a plan of the terraform code in exercise.
